<?php

namespace KDA\Filament\Wimdy\Middleware;

use Closure;

class ForcePasswordChange
{
    protected array | null $except;

    public function __construct()
    {
        $this->except = config('kda.filament-wimdy.middleware.exceptions', ['/admin/password/reset']);
    }

    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

    public function handle($request, Closure $next)
    {
        if (session('weak_password') === true) {
            if (! $this->inExceptArray($request)) {
                $e = auth()->user()->email;
                auth()->logout();

                return redirect(route('password.forcerequest', ['e' => $e]));
            }
        }
        $user = auth()->user();
        if (auth()->user()->enabled == false) {
            auth()->logout();

            return redirect(route('filament.auth.login'));
        }

        return $next($request);
    }
}
