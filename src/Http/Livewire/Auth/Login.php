<?php

namespace KDA\Filament\Wimdy\Http\Livewire\Auth;

use Filament\Http\Responses\Auth\Contracts\LoginResponse;
use JeffGreco13\FilamentBreezy\Http\Livewire\Auth\Login as FilamentLogin;

class Login extends FilamentLogin
{
    protected function attemptAuth($data)
    {
        $auth = parent::attemptAuth($data);
        $response = app(LoginResponse::class);
        if ($auth instanceof $response) {
            $user = auth()->user();
            /* if ($user->enabled == false) {
                 $this->logout($request);
                 return false;
             }*/
            if ($user->force_password_change && empty($user->password_changed_on)) {
                session(['weak_password' => true]);
            } else {
                session(['weak_password' => false]);
            }

            return $response;
        }

        return $auth;
    }
}
