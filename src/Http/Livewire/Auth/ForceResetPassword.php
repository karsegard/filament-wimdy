<?php

namespace KDA\Filament\Wimdy\Http\Livewire\Auth;

use Filament\Forms;
use Filament\Http\Livewire\Concerns\CanNotify;
use Illuminate\Contracts\View\View;
use JeffGreco13\FilamentBreezy\FilamentBreezy;
use JeffGreco13\FilamentBreezy\Http\Livewire\Auth\ResetPassword;

class ForceResetPassword extends ResetPassword implements Forms\Contracts\HasForms
{
    use Forms\Concerns\InteractsWithForms;
    use CanNotify;

    public $email;

    public $token;

    public $password;

    public $password_confirm;

    public bool $isResetting = false;

    public bool $hasBeenSent = false;

    public function mount($token = null): void
    {
        if (! is_null($token)) {
            // Verify that the token is valid before moving further.
            $this->email = request()->query('email', '');
            $this->token = $token;
            $this->isResetting = true;
        }
    }

    protected function getFormSchema(): array
    {
        if ($this->isResetting) {
            return [
                Forms\Components\TextInput::make('password')
                    ->label(__('filament-breezy::default.fields.password'))
                    ->required()
                    ->password()
                    ->rules(app(FilamentBreezy::class)->getPasswordRules()),
                Forms\Components\TextInput::make('password_confirm')
                    ->label(__('filament-breezy::default.fields.password_confirm'))
                    ->required()
                    ->password()
                    ->same('password'),
            ];
        } else {
            $email = request()->query('e');

            return [
                Forms\Components\TextInput::make('email')
                     ->default($email)

                    ->label(__('filament-breezy::default.fields.email'))
                    ->required()
                    ->email()
                    ->exists(table: config('filament-breezy.user_model')),
            ];
        }
    }

    public function render(): View
    {
        $this->getForms()['form']->fill();
        $view = view('kda-wimdy::forcereset-password');

        $view->layout('filament::components.layouts.base', [
            'title' => __('filament-breezy::default.reset_password.title'),
        ]);

        return $view;
    }
}
