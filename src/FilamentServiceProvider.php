<?php

namespace KDA\Filament\Wimdy;

use Filament\PluginServiceProvider;
use JeffGreco13\FilamentBreezy\Http\Livewire\Auth;
use KDA\Filament\Wimdy\Http\Livewire\Auth as MyAuth;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
        //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
        //    CustomWidget::class,
    ];

    protected array $pages = [
        //    CustomPage::class,
    ];

    protected array $resources = [
        //     CustomResource::class,
    ];

    public function configurePackage(Package $package): void
    {
        $package->name('filament-wimdy');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        Livewire::component(Auth\Login::getName(), MyAuth\Login::class);
        Livewire::component(MyAuth\ForceResetPassword::getName(), MyAuth\ForceResetPassword::class);
        //  Livewire::component(Auth\Login::getName(), MyAuth\Login::class);
    }
}
