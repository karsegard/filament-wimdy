<?php

use Illuminate\Support\Facades\Route;

Route::domain(config('filament.domain'))
    ->middleware(config('filament.middleware.base'))
    ->prefix(config('filament.path'))
    ->group(function () {
        Route::get('/password/forcereset', config('kda.filament-wimdy.password_forcereset_component_path'))->name(
            'password.forcerequest'
        );
    });
