<?php

// config for KDA/Filament\Wimdy
return [
    'middleware' => [
        'exceptions' => [
            '/admin/password/reset',
        ],
    ],
    'password_forcereset_component_path' => \KDA\Filament\Wimdy\Http\Livewire\Auth\ForceResetPassword::class,
];
