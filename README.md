# filament-wimdy

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/filament-wimdy.svg?style=flat-square)](https://packagist.org/packages/kda/filament-wimdy)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/filament-wimdy.svg?style=flat-square)](https://packagist.org/packages/kda/filament-wimdy)

![wimdy](wimdy.jpg)

## about

it's an extension for [jeffgreco13/filament-breezy](https://github.com/jeffgreco13/filament-breezy) that brings two features

    1: force the user to change his password
    2: disable user accounts without deleting them
## Installation



You can install the package via composer:

```bash

```composer require kda/filament-wimdy

### breezy
```bash

    php  artisan vendor:publish --tag="filament-breezy-config"
    php  artisan vendor:publish --tag="filament-breezy-migrations"
```


You will have to publish and run the migrations with:

```bash
php artisan vendor:publish --provider="KDA\Filament\Wimdy\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="KDA\Filament\Wimdy\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
<?php
// config for KDA/Filament\Wimdy
return [
    'middleware'=>[
        'exceptions'=>[
            '/admin/password/reset'
        ]
        ],
    'password_forcereset_component_path'=> \KDA\Filament\Wimdy\Http\Livewire\Auth\ForceResetPassword::class
];

```

## Filament

change your filament auth config to 

```php

    'auth' => [
        'guard' => env('FILAMENT_AUTH_GUARD', 'web'),
        'pages' => [
            'login' => \KDA\Filament\Wimdy\Http\Livewire\Auth\Login::class,
        ],
    ],

```

and add  **\KDA\Filament\Wimdy\Middleware\ForcePasswordChange::class** to your filament middleware like 
```php
    'middleware' => [
        'auth' => [
            Authenticate::class,
            \KDA\Filament\Wimdy\Middleware\ForcePasswordChange::class
        ],
    // rest of the config
    ]   

```

## User Model 

add 'force_password_change' and 'enabled' to your fillable attributes
```php
    protected $fillable = ['name', 'email', 'password','force_password_change','enabled'];
```


## Admin panel

Just add two toggles to your UserResource Form
```php
   Forms\Components\Toggle::make('enabled')->default(true),
   Forms\Components\Toggle::make('force_password_change')->default(true)
```
Just add two columns to your table
```php
    BooleanColumn::make('enabled'),
    BooleanColumn::make('active')
        ->getStateUsing(fn ($record): bool => ($record->force_password_change && !blank($record->password_changed_on) || !$record->force_password_change)  )
        ->trueColor(fn($record):string => !$record->force_password_change ? 'secondary': 'success'),
```
## Credits

- [Fabien Karsegard](https://github.com/karsegard)
- [All Contributors](contributors.md)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
## Security Vulnerabilities

Please review [our security policy](policy.md) on how to report security vulnerabilities.